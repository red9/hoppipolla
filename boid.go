package main

import (
	"github.com/g3n/engine/core"
	"github.com/g3n/engine/geometry"
	"github.com/g3n/engine/graphic"
	"github.com/g3n/engine/material"
	"github.com/g3n/engine/math32"
	"math/rand"
)

type Boid struct {
	mesh      *graphic.Mesh
	direction *math32.Vector3
	target    *math32.Vector3
	speed     float32
	nhood     []*Boid
}

func (b *Boid) velocity() *math32.Vector3 {
	v := b.direction.Clone().MultiplyScalar(b.speed)
	return v
}

func (b *Boid) position() *math32.Vector3 {
	vec := b.mesh.Position()
	return &vec
}

func initializeBoid(scene *core.Node, maxDist float32) *Boid {
	geom := geometry.NewSphere(0.1, 4, 4)
	mat := material.NewStandard(math32.NewColor("DarkBlue"))
	mesh := graphic.NewMesh(geom, mat)
	vec := randomUnit()
	pos := randomUnit().MultiplyScalar(rand.Float32() * maxDist)
	b := Boid{mesh, vec, math32.NewVec3(), 1, make([]*Boid, 0)}
	b.mesh.SetPositionVec(pos)
	scene.Add(mesh)
	return &b
}

func (b *Boid) accelerate(target, maxDelta, minSpeed, maxSpeed float32) {
	delta := target - b.speed
	if delta > maxDelta {
		delta = maxDelta
	}
	if delta < -maxDelta {
		delta = -maxDelta
	}
	speed := b.speed
	speed += delta
	if speed < minSpeed {
		b.speed = minSpeed
	} else if speed > maxSpeed {
		b.speed = maxSpeed
	} else {
		b.speed = speed
	}
}

func (b *Boid) move(r float32) {
	b.mesh.TranslateOnAxis(b.direction, r)
}

// rotates a boid towards a given vector by a maximum of max_angle radians.
func (b *Boid) rotateTowards(vec *math32.Vector3, maxAngle float32) {
	var axis math32.Vector3
	var q math32.Quaternion
	axis.CrossVectors(b.direction, vec)
	angle := b.direction.AngleTo(vec)
	if angle > maxAngle {
		angle = maxAngle
	}
	b.direction.ApplyQuaternion(q.SetFromAxisAngle(&axis, angle))
	b.direction.Normalize()
}

// returns a random unit vector.
func randomUnit() *math32.Vector3 {
	heading := rand.Float32() * math32.Pi * 2
	pitch := rand.Float32() * math32.Pi * 2
	vec := math32.NewVector3(
		math32.Sin(heading)*math32.Cos(pitch),
		math32.Sin(pitch),
		math32.Cos(heading)*math32.Cos(pitch))
	return vec
}

func (b *Boid) distanceTo(other *Boid) float32 {
	var dist math32.Vector3
	dist.SubVectors(b.position(), other.position())
	return dist.Length()
}
