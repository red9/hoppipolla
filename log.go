package main

import (
	"encoding/json"
	"fmt"
	"github.com/g3n/engine/math32"
	"io/ioutil"
	"os"
	"time"
)

type rawBoid struct {
	Position *math32.Vector3
	Velocity *math32.Vector3
}

func boidToRaw(boid *Boid) rawBoid {
	vel := boid.velocity()
	pos := boid.position()
	b := rawBoid{pos, vel}
	return b
}

func logState(boids []*Boid, time time.Time, config *Config) {
	rawBoids := make([]rawBoid, 0, len(boids))
	for _, boid := range boids {
		rawBoids = append(rawBoids, boidToRaw(boid))
	}
	timestamp := time.Format("20060102-150405")
	folder := fmt.Sprintf("logs/%s", timestamp)
	err := os.MkdirAll(folder, 0777)
	if err != nil {
		panic(err)
	}
	path := folder + fmt.Sprintf("/%d.json", config.Framecount)
	jsonMap := make(map[string]interface{})
	jsonMap["boids"] = rawBoids
	jsonMap["config"] = config
	j, _ := json.Marshal(jsonMap)
	err = ioutil.WriteFile(path, j, 0777)
	if err != nil {
		panic(err)
	}
}

type Stats struct {
	Mean   float32
	Stddev float32
}

func speeds(boids []*rawBoid) Stats {
	var sumSpeeds float32
	N := float32(len(boids))
	for _, boid := range boids {
		sumSpeeds += boid.Velocity.Length()
	}
	mean := sumSpeeds / N
	var sumDev float32
	for _, boid := range boids {
		sumDev += math32.Pow(boid.Velocity.Length()-mean, 2)
	}
	stddev := math32.Pow((sumDev / N), 0.5)
	return Stats{mean, stddev}
}

/*
analyze flocks - size, shape, speed differentials
Z,Y Z min-max
Flock size
Speed min max
*/
