package main

import (
	"encoding/json"
	"fmt"
	"github.com/g3n/engine/app"
	"github.com/g3n/engine/camera"
	"github.com/g3n/engine/core"
	"github.com/g3n/engine/gls"
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/light"
	"github.com/g3n/engine/math32"
	"github.com/g3n/engine/renderer"
	"github.com/g3n/engine/util"
	"github.com/g3n/engine/window"
	"io/ioutil"
	"math/rand"
	"os"
	"time"
)

type Config struct {
	AlignmentMul         float32
	CohesionMul          float32
	SeparationMul        float32
	SeparationThreshold  float32
	InitialBoids         int
	InitialBoidDist      float32
	AreaLimit            float32
	Speed                float32
	Acceleration         float32
	Rotation             float32
	MinSpeed             float32
	MaxSpeed             float32
	LoggingInterval      int
	Debug                bool
	NhoodType            string
	NhoodSize            int
	NhoodThreshold       float32
	NhoodRefreshInterval int
	Framecount           int
}

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		panic(fmt.Sprintf("Usage: %s $config-file", os.Args[0]))
	}
	configFile, _ := os.Open(args[0])
	configBytes, _ := ioutil.ReadAll(configFile)
	var config Config
	json.Unmarshal(configBytes, &config)
	startTime := time.Now()
	rand.Seed(startTime.Unix())
	// Create application and scene
	a := app.App()
	scene := core.NewNode()

	// Set the scene to be managed by the gui manager
	gui.Manager().Set(scene)

	// Create perspective camera
	cam := camera.New(1)
	cam.SetPosition(0, 0, 3)
	scene.Add(cam)

	// Set up orbit control for the camera
	camera.NewOrbitControl(cam)

	// Set up callback to update viewport and camera aspect ratio when the window is resized
	onResize := func(evname string, ev interface{}) {
		// Get framebuffer size and update viewport accordingly
		width, height := a.GetSize()
		a.Gls().Viewport(0, 0, int32(width), int32(height))
		// Update the camera's aspect ratio
		cam.SetAspect(float32(width) / float32(height))
	}
	a.Subscribe(window.OnWindowSize, onResize)
	onResize("", nil)

	boids := make([]*Boid, config.InitialBoids)
	for i := 0; i < config.InitialBoids; i++ {
		boid := initializeBoid(scene, config.InitialBoidDist)
		boids[i] = boid
	}

	// Create and add lights to the scene
	scene.Add(light.NewAmbient(&math32.Color{1.0, 1.0, 1.0}, 0.8))
	pointLight := light.NewPoint(&math32.Color{1, 1, 1}, 5.0)
	pointLight.SetPosition(1, 0, 2)
	scene.Add(pointLight)

	// Set background color to gray
	a.Gls().ClearColor(0.5, 0.5, 0.5, 1.0)

	f := util.NewFrameRater(60)

	//initialize sliders
	alignmentSlider := gui.NewHSlider(160, 20).SetScaleFactor(0.75).SetValue(config.AlignmentMul)
	alignmentSlider.SetText(fmt.Sprintf("alignment (%.4f)", config.AlignmentMul))
	cohesionSlider := gui.NewHSlider(160, 20).SetScaleFactor(0.75).SetValue(config.CohesionMul)
	cohesionSlider.SetText(fmt.Sprintf("cohesion (%.4f)", config.CohesionMul))
	separationSlider := gui.NewHSlider(160, 20).SetScaleFactor(0.075).SetValue(config.SeparationMul)
	separationSlider.SetText(fmt.Sprintf("separation (%.4f)", config.SeparationMul))
	alignmentSlider.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		m := alignmentSlider.Value()
		alignmentSlider.SetText(fmt.Sprintf("alignment (%.4f)", m))
		config.AlignmentMul = m
	})
	cohesionSlider.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		m := cohesionSlider.Value()
		cohesionSlider.SetText(fmt.Sprintf("cohesion (%.4f)", m))
		config.CohesionMul = m
	})
	separationSlider.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		m := separationSlider.Value()
		separationSlider.SetText(fmt.Sprintf("separation (%.4f)", m))
		config.SeparationMul = m
	})

	sliderPanel := gui.NewPanel(200, 100)
	sliderPanel.Add(alignmentSlider)
	alignmentSlider.SetPosition(10, 10)
	sliderPanel.Add(cohesionSlider)
	cohesionSlider.SetPosition(10, 40)
	sliderPanel.Add(separationSlider)
	separationSlider.SetPosition(10, 70)
	scene.Add(sliderPanel)

	//pause button
	paused := false
	pauseButton := gui.NewButton("Pause")
	pauseButton.SetSize(30, 30)
	pauseButton.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		paused = !paused
	})
	restart := false
	restartButton := gui.NewButton("Restart")
	restartButton.SetSize(30, 30)
	restartButton.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		restart = true
	})

	btnPanel := gui.NewPanel(200, 200)
	btnPanel.SetPosition(10, 100)
	btnPanel.Add(pauseButton)
	btnPanel.Add(restartButton)
	restartButton.SetPosition(60, 0)
	scene.Add(btnPanel)

	// Run the application
	a.Run(func(renderer *renderer.Renderer, deltaTime time.Duration) {
		f.Start()
		a.Gls().Clear(gls.DEPTH_BUFFER_BIT | gls.STENCIL_BUFFER_BIT | gls.COLOR_BUFFER_BIT)
		renderer.Render(scene, cam)
		if restart == true {
			for _, b := range boids {
				b.mesh.Dispose()
			}
			boids = make([]*Boid, config.InitialBoids)
			for i := 0; i < config.InitialBoids; i++ {
				boid := initializeBoid(scene, config.InitialBoidDist)
				boids[i] = boid
			}
			restart = false
			paused = true
		}
		if !paused {
			for _, b := range boids {
				target := b.doRules(boids, &config)
				b.target = target
			}
			for _, b := range boids {
				b.rotateTowards(b.target, config.Rotation)
				b.accelerate(b.target.Length(), config.Acceleration, config.MinSpeed, config.MaxSpeed)
				b.move(config.Speed)
			}
			if config.Framecount%config.LoggingInterval == 0 {
				logState(boids, startTime, &config)
			}
			config.Framecount += 1
		}
		f.Wait()
	})
}
