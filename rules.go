package main

import (
	"github.com/g3n/engine/math32"
)
import "sort"

func averageVel(boids []*Boid) *math32.Vector3 {
	sum := math32.NewVec3()
	for _, b := range boids {
		sum.Add(b.velocity())
	}
	return sum.MultiplyScalar(1.0 / float32(len(boids)))
}

func averagePos(boids []*Boid) *math32.Vector3 {
	sum := math32.NewVec3()
	for _, b := range boids {
		sum.Add(b.position())
	}
	return sum.MultiplyScalar(1.0 / float32(len(boids)))
}

func (b *Boid) threshold(boids []*Boid, threshold float32) []*Boid {
	nhood := make([]*Boid, 0, 8)
	for _, other := range boids {
		if b.distanceTo(other) < threshold {
			nhood = append(nhood, other)
		}
	}
	return nhood
}

func (b *Boid) nClosest(boids []*Boid, n int) []*Boid {
	if len(boids) < n {
		return boids
	}
	nhood := make([]*Boid, len(boids))
	copy(nhood, boids)
	sort.Slice(nhood, func(i, j int) bool {
		return b.distanceTo(nhood[i]) < b.distanceTo(nhood[j])
	})
	return nhood[:n]
}

func (b *Boid) visual(boids []*Boid, threshold float32) []*Boid {
	nhood := make([]*Boid, 0, 8)
	v := b.direction
	for _, other := range boids {
		p := other.position().Sub(b.position())
		if v.AngleTo(p) < threshold {
			nhood = append(nhood, other)
		}
	}
	return nhood
}

func (b *Boid) separation(boids []*Boid) *math32.Vector3 {
	sum := math32.NewVec3()
	for _, other := range boids {
		d := b.distanceTo(other)
		sum.Add(b.position().Sub(other.position()).DivideScalar(d * d))
	}
	return sum
}

func (b *Boid) controlSpeed(multiplier float32) *math32.Vector3 {
	speedDelta := (1 - b.speed)
	return b.velocity().MultiplyScalar(speedDelta * multiplier)
}

func (b *Boid) limitArea(threshold float32) *math32.Vector3 {
	if b.position().Length() > threshold {
		return b.position().SetLength(-1.2)
	}
	return math32.NewVec3()
}

func (b *Boid) doRules(boids []*Boid, config *Config) *math32.Vector3 {
	nType := config.NhoodType
	var nhood []*Boid
	if len(b.nhood) == 0 || config.Framecount%config.NhoodRefreshInterval == 0 {
		if nType == "nClosest" {
			nhood = b.nClosest(boids, config.NhoodSize)
		} else if nType == "threshold" {
			nhood = b.threshold(boids, config.NhoodThreshold)
		} else if nType == "visual" {
			nhood = b.visual(boids, config.NhoodThreshold)
		}
		b.nhood = nhood
	} else {
		nhood = b.nhood
	}
	bVel := b.velocity()
	bPos := b.position()
	alignment := averageVel(nhood).Sub(bVel).MultiplyScalar(config.AlignmentMul)
	cohesion := averagePos(nhood).Sub(bPos).SetLength(1.0).Sub(bVel).MultiplyScalar(config.CohesionMul)
	separation := b.separation(nhood).MultiplyScalar(config.SeparationMul)
	speed := b.controlSpeed(0.05)
	area := b.limitArea(config.AreaLimit)
	target := b.velocity()
	target.Add(alignment).Add(cohesion).Add(separation).Add(speed).Add(area)
	return target
}
